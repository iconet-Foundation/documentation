.. _iframe-sandboxing:

=====================================
4.1 Iframe Permissions and Sandboxing
=====================================
Fallback-iframes are used to display and encapsulate iconet messages in HTML
format.
The following sections describe how communication and encapsulation between
parent and the fallback-iframe can be accomplished, different levels of isolation,
and the permissions used to describe them in the :ref:`interpreter manifest
<interpreter-manifests>`.

.. seealso::

  You can find an example document, an embedding application that embeds an
  iframe, enforces restrictions, and initiates communication `here
  <./_static/code-examples/iframe-sandboxing/parent.html>`_.

  Since by default, fallback-iframes are not allowed to connect to endpoints on
  the web, all data needs to be embedded within the iframe's HTML. You can see
  an example of an embedded image `here
  <./_static/code-examples/iframe-blobs/html-with-image-data.html>`_.

.. _iframe-permissions:

4.1.1 Fallback-iframe Permissions
=================================
By default, an iframe is not allowed to establish connections to the outside
world. By requesting permissions, the iframe communication restrictions may be
alleviated.

.. csv-table::
    :header: "Name", "Range", "Description"
    :widths: auto

    "
    ``allowedSources``", "
    array of `CSP source values
    <https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources>`_", "
    Sources to URIs the iframe may connect to. Defaults to empty.
    "
    "
    ``allowInteractions``", "
    ``boolean``", "
    If the fallback-iframe is allowed to send interactions via the embedding
    application. If true, must be set along with ``interactionCooldown``.
    Default is ``false``.
    "
    "
    ``interactionCooldown``", "
    ``number``", "
    The number of seconds the fallback-iframe needs to wait until it can send
    another interaction. Requires ``allowInteractions`` to be set to ``true``.
    "
    "
    ``allowContentRequest``", "
    ``boolean``", "
    Experimental: Allow the iframe to make requests through the embedding
    application, to check if there are updates to the packet using the
    :ref:`interaction pull method <interaction-pull-method>`.
    "


.. _iframe-isolation-levels:

4.1.2 Levels of Isolation
=========================
WIP-Level: 3 (figuring out which permissions to offer by spec and when to apply
which ones is still in debate)

Depending on the trust established between receiver and sender, the capabilities
of the iframe may be restricted to varying extent. The following subsections
discuss different levels of isolation, beginning with the strictest.

Disabled Remote Resource Access
-------------------------------
For this level, the fallback-iframe will receive the iconet packet but is not
allowed to send information away or request additional content (e.g. images).
All resources have to reside in the HTML of the fallback-iframe, e.g. images must
be encoded in the HTML. Encoded binary data should reside at the bottom of the
document to enable pre-rendering the rest of the document.

Controlled Remote Communication
-------------------------------
Sending packets from the fallback-iframe is allowed but only by using a
channel controlled by the embedding application.

This way, the parent can restrict the frequency of messages sent and restrict
the addressees to the sender audience. See the section on
:ref:`sending interactions <iframe-sending-interactions>`

TODO: Sort out specification for allowed addressee restriction.

Enabled Remote Resource Fetching
--------------------------------
TODO: enhance CSP configuration description.

The fallback-iframe is allowed to fetch resources from certain sources that
are whitelisted in the iconet packet.
The parent applies a corresponding ``default-src`` Content Security Policy.

Additional CSP directives do not seem to provide additional security
(e.g. using a more liberal ``img-src`` compared to ``default-src`` cannot
prevent the script from communicating through image resources)

The degree of trust between sender and receiver must be fairly high, since
a lot of metadata (e.g. IP, browser fingerprint, time of rendering the message,
user behavior) can be exposed this way.

Trust
-----
There are at least three imaginable sources for interpreters and multiple
authorities for determining the level of trust that a fallback-iframe should be
granted.

Sources for interpreters:

- sending client (linked in iconet packet)
- receiving client (either from cache or configuration)
- community repository

Authorities for establishing trust:

- A designated third party authority or community repository
- The interpreter domain / organization
- The sending user
- The receiving user by review

Most times, there is to some degree a trade-off between usability and privacy.
We want to make this as small as possible while preserving a high degree of
privacy.
Therefore, a fallback-iframe should limit its level of isolation to the minimum
required. This will improve usability for users, since they do not have to
perform a review to elevate the fallback-iframe's permission.

**Establishing trust is a critical component and needs additional elaboration
and agreement among participating parties. This is a big TODO and detailed
specification seems out of scope for now.**


.. _sandboxing-iframes:

4.1.3 Sandboxing Iframes
========================
WIP-Level: 1

Fallback-iframes must be sandboxed to enforce isolation and prevent execution of
arbitrary code in the parent container and communication to the outside world.

Content Security Policy
-----------------------
To prevent iframes from unmonitored communication in browser-based contexts, a
`Content Security Policy (CSP)
<https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP>`_ can be employed.
Below, you can find three approaches to enforce CSPs on iframes.

Option 0: Using the Iframe's ``csp`` Attribute
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
As of 2022-11-23, there exists a `draft spec
<https://developer.mozilla.org/en-US/docs/Web/API/HTMLIFrameElement/csp>`_
to enforce the CSP on an iframe:

.. code:: html

  <iframe csp="default-src 'none'">

It is however not supported by Firefox and Safari.

Option 1: Add a CSP to the Iframe's HTML
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(1) Fetch the fallback-iframe's HTML by script
(2) Modify the fetched HTML appending a ``meta`` tag
    at the top of the HTML head:

    .. code:: html

      <html>
        <head>
          <meta http-equiv="Content-Security-Policy" content="child-src 'none';" />
          ...

(3) Set the modified HTML to the iframe's ``srcdoc`` attribute.

.. warning::

  You *must* make sure that no scripts or remote content precedes the
  meta tag. The CSP only takes effect, once the tag is parsed. See the `spec
  <https://w3c.github.io/webappsec-csp/#meta-element>`_ for more details.

Option 2: Use a proxy that sets the appropriate Content Security Policy headers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Instead of using the URI to the fallback-iframe directly, use a trusted proxy that
fetches the iframe HTML and sets the desired CSP headers on response.

