.. _glossary:

========
Glossary
========

.. _glossary-fallback-iframe:

Fallback Iframe (Interpreter)
=============================
The heart of fallback data interpretation.
Iframes are HTML elements that can embed external html content in an isolated
sandbox. |br|
The fallback iframe is used by the :ref:`embedding application
<glossary-embedding-application>` which is not capable of interpreting a
:ref:`foreign packet <glossary-foreign-protocol>`.
The fallback iframe communicates with the embedding application in a
standardized manner, to pass the packet to the iframe and enable further
interaction.

There are two types of :ref:`fallback iframes <fallback-iframes>`:
:ref:`Presenter Iframes <glossary-presenter-iframe>` and
:ref:`Translator Iframes <glossary-translator-iframe>`

.. _glossary-presenter-iframe:

:ref:`Presenter Iframes <glossary-presenter-iframe>`
render a HTML presentation of the packet passed to it and may allow user
interaction with the iframe.

.. _glossary-translator-iframe:

:ref:`Translator Iframes <glossary-translator-iframe>`
function in the same way, however, they do not render a presentation but return
the given packet data in a different format back to the embedding application,
hence "translate" the packet.
For example, a client received a packet of type ``text/markdown``. The packet
has translator iframes referenced to convert ``text/markdown`` to ``image/jpeg``
and ``text/html``. Since the application doesn't know how to interpret markdown,
it uses the translator to convert it to a HTML document, sanitizes the HTML, and
shows it to the user.


.. _glossary-embedding-application:

Embedding Application
=====================
An embedding application is an application that wants to display content that it
cannot interpret itself but that has iconet metadata attached. It therefore
passes the content (a packet) to a fallback iframe which renders a presentation.


.. _glossary-foreign-protocol:

Foreign Protocol
=========================
A protocol that is not (natively) supported by a given :ref:`embedding
application <glossary-embedding-application>`.


.. _glossary-foreign-packet:

Foreign Packet
==============
A packet that is in the format of a :ref:`foreign protocol
<glossary-foreign-protocol>`.



.. _glossary-interpreter-manifest:

Interpreter Manifest
====================
A JSON-LD object that provides information on fallback iframes, their
supported input packet types, their location, and their security-relevant
permission requirements.


Packet
======
A collection of data used to communicate from one application to another.

WIP-Level
=========
Some sections begin with a ``WIP-Level:`` indicator.
The Work-In-Progress-Levels is a guide as to how much mature the given section
is. Where WIP-Level 5 means "absolutely just some notes or thoughts" and
WIP-Level 0 means "we elaborated extensively and came up with something we feel
confident enough to put in our prototype implementation".


Status
======
TODO:
Somethink like
a value of ``implemented``, ``idea``, ``in discussion``.
