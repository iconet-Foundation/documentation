.. _challenges-approaches:

================================
Challenges & Approach Discussion
================================

Connecting (partially) incompatible networks opens up questions. For example:

- How to address a user / audience on a remote platform?
- How to present their content?
- How do systems communicate compatibility support?
- How can interactions (i.e. a poll or reactions) be supported?
- How / in what schema is content formatted?
- How does the API work?
- How to enable secure communication?
- **Finding consensus & acceptance in the community**

This page is a very incomplete list of challenges and discussions on possible
solution pathways for solving those issues.


Presentation
============
- When to fall back to non-native presentation of content?
- What format does the fallback presentation have?
- How is fallback presentation generated - on the receiver side by using the
  sender's platform-native message and transforming it in a receiver-understood
  target format?
- What, if the client understands parts of the message (e.g. an attached vCard)
  but does not know how to present the whole information?

For presentation on the client side, we identify three approaches which are not
mutually exclusive but combinable.

1. Using format interpreters that support logic and which take any kind of
   message to create a supported presentation (flexible but complex).
   This is the most flexible approach and where Iconet is heading towards.
2. Using a very basic common default format supported by all clients, e.g. plain
   text (which has to be sent along the original packet or on request).
   This could be a useful fallback for clients that do not supported
   HTML+Javascript.
3. Using content negotiation to find out about the intersection of supported
   formats between sender and receiver application.
   Disadvantage: what happens if there are no common formats?
   A more detailed elaboration of the approaches is given in the subsections.

A possible example procedure combining the approaches could look as follows:

1. The receiving client has received a packet that it cannot present to the
   user.
   The packet has a plain text presentation attached as well (as last resort).
2. The receiving client supports HTML and decides to fetch a format interpreter
   HTML document linked in the packet. Alternatively, the document could reside
   in the packet. (Optionally: When fetching the document, content negotiation
   could also result in a different kind of interpreter that, for certain
   protocols, can translate the packet into a format understood by the client.)
3. The format interpreter HTML document can be embedded as an iframe
   by the receiving client. The receiving client passes the packet to the
   format interpreter HTML document which renders a HTML representation.

Format Interpretation
---------------------
The idea of providing an interpreter is the following:
If the receiver application is not able to present the received packet in
the sender application's format, the interpreter can be used to translate the
information that was not understood by the receiver application.

The interpreter can be thought of as a converter that takes the sender's native
packet and generates a markup that the client can render. To support interactive
content, the interpreter could be an iframe taking data passed to it to display
a widget.

Ideally, no sensitive data should be exposed to anyone but the receiver in that
process.
Therefore, the question arises as to how the interpreter information is
transmitted to the receiver application. By request (and to whom?) if the
packet is not understood or in advance by the sender application?

The advantage of using an interpreter is that the sender does not need to
provide (another) target format. The transformer can be developed separately.
The message does not need to be re-requested if the receiver application does
not understand the format but only a transformer.

In some cases, certain parts of the foreign packet might be understood by the
receiving client. In that case, the receiving client is in charge of deciding
if to render the fallback and how handle the packet.

Potential Examples
~~~~~~~~~~~~~~~~~~
An isolated iframe (with no access to external (internet) resources)
receives the packet that's not able to be presented by the receiver
application's native methods.

In the iframe, the interpreter can execute JavaScript to process the data
and render a presentation for the user.
Outgoing communication could be channeled through an interface controlled
by the client, that acts as a proxy and filters requests to external resources.

Default Formats
---------------
Alternatively, instead of providing an interpreter, the message sent could be
formatted in a very generic way understood by any client (e.g. plain text) or
multiple formats. See `this matrix MSC <https://github.com/matrix-org/matrix-spec-proposals/blob/matthew/msc1767/proposals/1767-extensible-events.md>`_
as a source of inspiration for example. The MSC proposes to append a markup for
a message in multiple formats, each with a mimetype attached. The client is
supposed to present the first format supported.

.. code:: json

  "m.markup": [
    {"body": "<h1>Hello there!</pre>", "mimetype": "text/html"},
    {"body": "Hello there!",           "mimetype": "text/plain"}
  ]

Content Negotiation
-------------------
In the previous section, there were multiple formats of the same message were
sent in one packet. In some cases, it might be useful to perform
content negotiation.

The client or the open inbox could ask to receive the message in a preferred
format that both support. Downside: The inbox might not know in advance which
formats the client fetching the messages supports.

A different form of content negotiation could be used when the receiving client
requests a format interpreter. The endpoint that provides the format
interpreters could support content negotiation for different target formats
(that would not even need to convert to a markup but a client-native packet
format).

tl;dr
-----
Options:

1. The Iconet packet contains the sender-native packet.
  
   - The iconet packet contains a link to an endpoint providing interpreters
     (e.g. iframes) that the client can use to convert and render the packet.
     Using content negotiation, different target formats could be requested.
     Caching and security considerations should be considered.
   - Instead of providing an endpoint for the interpreter, the needed data could
     be transmitted in the iconet packet as well but reducing caching options.
   - The interpreters's code should be provided with a checksum to reduce
     attack vectors.
2. The iconet packet contains multiple content formats of which at least one
   should be supported by the receiving application (probably plaintext).
3. Both of the above


Content Interaction
===================

- How are interactions (e.g. a like or a comment) made
  available / communicated back?
- Should iframes be allowed to communicate back themselves (via their own
  requests) or should they request their parent window to communicate?
- Is an iconet (interaction) packet processed by the client or the inbox server?
- Can interactions be authorized and how?
- Use cases:

  - Use Case: Bob can like Alice's post, but Claire
    (to whom the post was forwarded), can't
    Can authorization to access formats be delegated to receivers?
  - (How) can messages be forwarded?
    Can formats handle 'forwarding' internally (not part of the standard)?
  - Should we aim for standard auth mechanisms (e.g. OAuth)?
  - Use Case?: A shared document is sent to Bob and a preview is rendered
    but Claire, to whom the post was forwarded, should not see the preview.
    (How) can access be granted or requested?
  - A public post that is sent to Bob in an iconet packet is supposed be
    be commentable by Bob only.

Option 1: Iframe communicates with native Apis
---------------------------------
The main approach, as documented in :ref:`4.2.4. Receiving Updates /  Sending Interactions <iframe-sending-interactions>`

Option 2: Tunneled communication between iframe & embedding Application
---------------------------------

The fallback-iframe can communicate over a controlled channel provided by its
embedding application. Packets sent over the controlled channel are sent back to
the sender of the original packet. Since transport is handled by the embedding
application, this reduces overhead for the fallback-iframe creators and moves
questions of signing and authenticating as well as encrypting packets there.
Therefore, for end-to-end encrypted communication, this has the advantage of
minimizing  possible data-leakage to third-parties.
On the other hand, it is less flexible.

The interaction packet is a JSON-LD-formatted object,
the **format is standardized**:

.. code:: JSON-LD

  {
    "@context": "https://ns.iconet-foundation.org#",
    "@type": "Interaction",
    "@id": "<an IRI identifying the packet>",
    "timestamp": "<ISO 8601 timestamp>",
    "originalPacket": "<the IRI identifying the original / target packet>",
    "payload": "<custom data>"
  }

The interaction packet's payload is then forwarded back to the sender of the
original packet.


Modes of Communication & Security
---------------------------------
A container in the receivers application is used to wrap the interpreter and its
packet presentation.

**Possible proposal**: Multiple trust levels

1. container is completely isolated
2. container may communicate via its parent host in a limited fashion
3. container may communicate by itself

The user or the client is in charge of allowing to elevate a message's
trust level.

Embedded (non-native) content MUST NOT be allowed to alter their parents' state
directly.
Communication between the parent application and the embedded content must be
supervised.

Here, we illustrate three concepts on how interactions or
updates can reach the client.

1. :ref:`Option 1) <interaction-native-pull>` would support neither method but only
   allow trusted fallback-iframes to connect to sources using the available
   javascript interfaces, e.g. ``fetch``. Thus, no further specification on the
   iconet side would be necessary. See :ref:`Sending Interactions
   <iframe-sending-interactions>` for the related discussion.
2. :ref:`Option 2) <interaction-push-method>` would allow actors to send updates
   (e.g. a message was edited) for packets they previously sent to the inbox.
   This method can be thought of as push-like.
3. :ref:`Option 3) <interaction-pull-method>` would allow clients to poll for
   updates at the source, if needed.
   This method can be though of as pull-like.

.. _interaction-native-pull:

5.1. Option 1: Fallback-iframe Self-Governing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In analogy to the `previous section
<Fallback-iframe Establishes Connections Itself>`_, the iframe connects to a
whitelisted source by itself and polls for updates. The process is thus not
iconet-specific.

.. _interaction-push-method:

5.2. Option 2: Push-Based
~~~~~~~~~~~~~~~~~~~~~~~~~
If updates to a packet (i.e. someone commented on a post) arise, the embedding
application receives an interaction packet / update to a packet.
The embedding application passes the payload over to the fallback-iframe.

Note that if transport is not handled by a common iconet protocol, the
schema and format may vary. The payload however must not be altered during
transport
This is in analogy to the `regular packets
<Metadata for an Iconet-Supporting Packet>`_.

.. code:: JSON-LD

    {
        "@context": "https://ns.iconet-foundation.org#",
        "@type": ["Packet", "Update"],
        "@id": "<id of packet>",
        "refersTo": "<id of original packet>",
        "actor": "<sender>",
        "to": ["<addressee>"],
        "content": ["<... content data array, same as in the regular packets>"]
    }

.. _interaction-pull-method:

(5.3. Option 3: Pull-Based)
~~~~~~~~~~~~~~~~~~~~~~~~~~~
WIP-Level: 4

A request to ask for updates could look as follows. Note that if transport is
not handled by a common iconet protocol, the the schema and format may vary.

.. code:: JSON-LD

    {
        "@context": "https://ns.iconet-foundation.org#",
        "@type": "UpdateInquiry",
        "originalId": "<id of original message>",
        "sender": "<sender address of this packet>",
        "to": ["<address of the original packets sender>"]
    }

The response would be a regular iconet packet with an additional ``updateTo``
field that contains the original packet's identifier. Alternatively, the packet
could maintain the same id and the packet could have a timestamp and an update
count.

One option would be that the client invalidates the old message from thereon.
Alternatively, the client could pass the payload of the response to the iconet
iframe, using the established message channel between client and fallback-iframe.

A response to an update inquiry could look as follows.  If status ``NoChange``
is set, the packet does not need to have type Packet and the corresponding
payload field.

.. code:: JSON-LD

    {
        "@context": "https://ns.iconet-foundation.org#",
        "@type": ["Packet", "Update"],
        "@id": "<id of packet, if packet has update>",
        "predecessor": "<id of original message>",
        "status": "<either Update or NoChange.>",
        "sender": "<sender address of this packet>",
        "to": ["<address of the original packets sender>"],
        "payload": "<native data>"
    }


Authentication
--------------
The authenticity of an actor could be established if communication is channeled
through the parent host and the parent host's authenticity to the original
sender is given.
In the end, that leads to a situation where trust needs to be established
between platforms and users, where the respective platform ensures the
authenticity of senders and receivers.

A separate, dedicated mechanism seems out of scope for a basic protocol.
Even the whole web community hasn't agreed on a single working interoperable
spec that fulfills everyone's use case.



