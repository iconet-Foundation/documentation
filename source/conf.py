# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'iconet documentation'
copyright = '2024, Iconet Foundation'
author = 'Iconet Foundation'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# templates_path = ['_templates']
exclude_patterns = []

html_static_path = ["static"]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
# html_static_path = ['_static']

html_theme_options = {
  'navigation_depth': 4,
  'collapse_navigation': False,
  'vcs_pageview_mode': 'edit'
}

# -- Macros ------------------------------------------------------------------
rst_prolog="""
.. |br| raw:: html

  <br/>
"""

github_url="https://codeberg.org/iconet-Foundation/documentation/"

