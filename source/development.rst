.. _development:

=============
Development + Demo
=============

Non-Exhaustive Overview of the 3 current Demo-Projects


Mastodon with iconet Extension
==============================

Mastodon is a micro blogging service that supports `ActivityPub <https://www.w3.org/TR/activitypub/>`_.

As a proof-of-concept, we added `iconet-specific metadata <https://docs.iconet-foundation.org/en/latest/specification.html#required-iconet-meta-data>`_ to status objects.

This fork can receive `ActivityPub <https://www.w3.org/TR/activitypub/>`_-formatted objects with iconet metadata and utilize it to render a fallback presentation. Such objects can be sent with our `Funkwhale fork <https://codeberg.org/iconet-Foundation/funkwhale>`_ or with our custom prototype, `NetA <https://codeberg.org/iconet-Foundation/prototype-ExampleNetA>`_ with packets being transported via a `bridge <https://codeberg.org/iconet-Foundation/bridge>`_.

All outgoing activities of this instance append the iconet metadata. Currently, fallback presentations for native mastodon toots and markdown formatted text are implemented.

For the current state of the iconet spec, check `our readthedocs <https://docs.iconet-foundation.org>`_.

Screenshots
-----------

.. figure:: ../images/funkwhaleInMastodon.png
   :alt: This instance can play Funkwhale audio tracks that contain iconet metadata.

   This instance can play Funkwhale audio tracks that contain iconet metadata.

.. figure:: ../images/screenshotMastodonMarkdown.png
   :alt: Mastodon toot with markdown formatted text.

   Mastodon toot with markdown formatted text.

.. figure:: ../images/screenshotMastodonNetA.png
   :alt: An example network that embeds a toot via fallback iframe.

   An example network that embeds a toot via fallback iframe.


Iconet - Funkwhale
==================

Funkwhale is an audio service that supports `ActivityPub <https://www.w3.org/TR/activitypub/>`_.
As a proof-of-concept, we added `iconet-specific metadata <https://docs.iconet-foundation.org/en/latest/specification.html#required-iconet-meta-data>`_ to audio tracks.

When iconet-supporting clients receive an `ActivityPub <https://www.w3.org/TR/activitypub/>`_-formatted audio track packet, they can use the iconet metadata to render a fallback presentation.
Currently, those can be viewed with our `modified Mastodon fork <https://codeberg.org/iconet-Foundation/mastodon>`_ and with our custom prototype, `NetA <https://codeberg.org/iconet-Foundation/prototype-ExampleNetA>`_ with packets being transported via a `bridge <https://codeberg.org/iconet-Foundation/bridge>`_.

For the current state of the iconet spec, check `our readthedocs <https://docs.iconet-foundation.org/>`_.

Note: For the setup, in the file ``manifest.jsonld``, you additionally need to prefix the ``@id`` URIs with the funkwhale hostname you are using and add the hostname to the field ``allowedSources``.

Iconet integration into ExampleNetA
====================================

This is a basic network, with a low feature set and the code uses no frameworks. This allows for a quite untangled
demonstration on how to integrate the iconet technology.
ExampleNetA is taken from: https://github.com/yaswanthpalaghat/Social-Network-using-php-and-mysql

---

.. image:: http://iconet-foundation.org/images/BMBF_en.png
   :alt: BMBF

From September 2022 to February 2023 this project received funding
from the German Ministry of Education and Research.

