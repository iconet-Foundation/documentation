.. _usecase:

=============
Use Case
=============
This page aims to give a better understanding off the general purpose of the project, aswell as by whom and how it should be adapted.


Summary
~~~~~~~~

**Interconnectivity for social networks** aims to ensure that users of different networks based on different technologies are able to interact.

This is achieved by establishing a common fallback mechanism, both within different decentral networks, and across.


Pretext: Interoperability
==================

There is a variety of open social networks based on protocols
which are open source and therefore freely accessible.
Many of those networks based on the same or a similar protocol, have chosen to federate,
this means allowing a cross communication for users.
This is called interoperability. A prominent example for such a protocol is
`ActivityPub <https://www.w3.org/TR/activitypub/>`_ which is implemented by `50+ Softwares <https://fedidb.org/software>`_ and connects millions of people.

**There are plenty of technologies and applications that wish to federate**

Problem: Incompatibilities
==================

The different applications for such protocols are developed by independent teams -
each of them with varying intentions and goals for the functionality of their
application. This creates a great diversity and ensures freedom of choice across the
network. But also it often results in technical incompatibility,
because different aspects of a protocol are used, different extensions are created,
or entirely different protocols or tools are implemented in the first place.
Due to those issues the general promise of open federated networks is not fully given across ends.
For the uninitiated user those limits are not comprehensible.

**While decentral systems are fully capable, developers of users applications often only implement subsets of possible formats and interaction methods. Therefore content by users off different applications often can not be presented.**


Solution: Interconnectivity
==================

In contrast to interoperability, which ensures broad functionality for networks,
with interconnectivity we focus on the communication capability between explicitly
incompatible systems. For many use cases, it is important that deeper functionality is
fully mapped across all networks involved. Wherever this does not work interconnectivity
ensures that communication is transmitted securely and presented properly.
It serves as a minimal fallback mechanism,
applications can use in addition to their core functionality.

**Having a fallback in place, ensures communication.**


Adoption
~~~~~~~~
By whom and how is interconnectivity to be adopted? Here we have an overview:

Inner Use case of interconnectivity
===============
Projects like the Fediverse, Matrix.org, SoLiD, BlueSky, XMPP each enable complex decentral datastructures.
The inner use case is for those systems, to include the fallback mechanism into their internal packet exchanges.

If they adopt the fallback to their protocol, they gain the following advantages:

- End to end communication within the protocol is ensured.
- Applications can become more diverse, because innovative formats can be adopted by anyone right away.
- The eco system may evolve, because older versions stay compatible with newer ones.

To adopt the fallback internally, the following measures need to be taken:

- for any format, a interpreter of the native package needs to be provided. (HTML)
- for any client, the displaying of an fallback interpreter needs to be provided. (Java Script)
- for any packet, the information on where to find the corresponding interpreter needs to be added. (JSON)

**The inner use case is necessary for decentral data exchanges, to avoid internal incompatibilities.**

Outer Use case
=========
Networks which adopted the fallback mechanism, can ensure communication across their network.
The fallback mechanism then can be especially effective, if also fallback means of transportation are adopted.
If e.g. from an endpoint in the fediverse a packet including fallback info can be delivered to an end point in Matrix, communication here can also be assured.
This is the outer use case, to use the fallback to acchieve interconnectivity across technical barriers.

If networks adopt the fallback transport, they gain the following advantages:

 - massivly extended connectivity for their users (in and out)
 - organical distribution of communication technologies, because they know can peacefully coexist.
 - a stable global communication network, available to anyone independent of technology.

To adopt the fallback externally, the following measures need to be taken (additional to the inner use case):

 - adopt the fallback means of addressing, packeting, encryption as a second layer of transport.

**The outer use case is the chance to bridge the gap between different communication technologies, for maximum independence of the users.**


Closing statement
=======

With interconnectivity, the former limitations of interoperability turn into strengths. While before different standards and implementations where competing in an incompatible manner,
they now can coexist in a user friendly way. New implementations can arise and spread organically, users can openly decide, which technology to use.

.. note::

  The restraint of interconnectivity to the bare technical minimum is not meant
  to span a meaningful social network of its own. Of course deeper functionality and
  security mechanisms are to be adapted, to create a meaningful and save user
  experience. Each actor who initiates communication, by choosing the software
  and the technology, also chooses the more detailed conditions of communication.
  If actors on the receiving end of such communication do not agree with any of these
  conditions, it should be blocked. Interconnectivity aims to remove arbitrary
  technical barriers, so that developers and users have the chance to enforce
  barriers by design and by choice. When actors have contradicting communication
  desires, that's not a technical issue and couldn't be solved by any protocol.

**interoperability > interconnectivity > no connectivity.**

