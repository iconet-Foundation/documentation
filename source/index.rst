===================================
Welcome to the Iconet Documentation
===================================

This document outlines the proposed Iconet (interconnected networks) specification,
which describes a minimal fallback mechanism to display and interact with
content from natively unsupported social networks and clients.

This documentation is an early structural draft of a standard for
interconnectivity. The standard recommendation is yet to be created through peer reviews and an open community process. Different parts can
potentially be subject to radical change in the future.

If you have any questions, feedback, or ideas, don't hesitate to `contact
<https://iconet-foundation.org/about>`_ or open an issue / pull request `on
codeberg <https://codeberg.org/iconet-Foundation/documentation>`_.

What's here?
====================
-  A summary of the :ref:`Use Case <usecase>` and the problems this project aims to attack

- :ref:`The Specification of.. <specification>`

  - data required to provide a presentation fallback or a packet translation
    (:ref:`Required Iconet Data for a Packet <required-iconet-meta-data>`)
  - manifests that contain the required data to create a fallback
    presentation or translation (:ref:`Interpreter Manifests
    <interpreter-manifests>`)
  - the usage of iframes to provide a packet presentation fallback or to
    translate a packet, as well as the communication flow between iframe and
    embedding application (:ref:`Fallback-iframes <fallback-iframes>`)
  - methods and discussions on isolating iframes from the embedding application
    to prevent data leakage (:ref:`Iframe Permissions and Sandboxing
    <iframe-sandboxing>`)
-  The :ref:`Development<development>` process of the Demo projects

- :ref:`Challenges & Discussions <challenges-approaches>` outlines discussions
  we faced during development and possible alternatives for the
  spec.
- :ref:`Glossary <glossary>` explains the basic terms and components


Prototypes
==========
To demonstrate and experiment with the functionality of the spec, we developed a
prototype network and developed an extension to mastodon:

* `Prototype Network A <https://codeberg.org/iconet-Foundation/prototype-ExampleNetA>`_ ,
* `Mastodon Fork <https://codeberg.org/iconet-Foundation/mastodon>`_
* `Funkwhale Fork <https://codeberg.org/iconet-Foundation/funkwhale>`_

You can find all of our repositories `on Codeberg
<https://codeberg.org/iconet-Foundation>`_.


Website
=======
See the `Iconet Foundation Website <https://iconet-foundation.org>`_ for current
blog updates, explanatory materials and ways to contribute.


-----

.. image:: https://iconet-foundation.org/images/BMBF_en.png

From September 2022 to February 2023 this project receives funding
from the German Ministry of Education and Research.



.. toctree::
   :hidden:

   self
   usecase
   specification
   development
   challenges
   glossary
