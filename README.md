[![Documentation Status](https://readthedocs.org/projects/iconet-documentation/badge/?version=latest)](https://iconet-documentation.readthedocs.io/en/latest/?badge=latest)

# iconet Documentation

This is the repository for the iconet spec. You can find the latest version at https://docs.iconet-foundation.org/ which is hosted by
[readthedocs](https://readthedocs.org).

## Deployment

To generate the documentation on your local machine (pip and python required),
run:


```bash
git clone https://codeberg.org/iconet-Foundation/documentation.git
cd documentation
pip install --requirement=requirements.txt
make html
```
On Windows use:
```bash
git clone https://codeberg.org/iconet-Foundation/documentation.git
cd documentation
py -m pip install --requirement=requirements.txt
./make.bat html
```

-----

![](http://iconet-foundation.org/images/BMBF_en.png)

From September 2022 to February 2023 this project receives funding
from the German Ministry of Education and Research.